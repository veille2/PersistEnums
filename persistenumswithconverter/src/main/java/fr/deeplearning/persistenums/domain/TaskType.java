package fr.deeplearning.persistenums.domain;

public enum TaskType {

    FRONT("FRONTEND"),
    BACK("BACKEND"),
    RUNDEV("RUN"),
    OPS("DEVOPS");

    private final String code;

    TaskType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
