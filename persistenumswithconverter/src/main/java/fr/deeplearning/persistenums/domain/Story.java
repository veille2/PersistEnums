package fr.deeplearning.persistenums.domain;


import fr.deeplearning.persistenums.converter.TaskTypeConverter;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Story {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "type")
    @Convert(converter = TaskTypeConverter.class)
    private TaskType type;

    @Column(name = "title")
    private String title;
}
