package fr.deeplearning.persistenums.converter;

import fr.deeplearning.persistenums.domain.TaskType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

@Converter
public class TaskTypeConverter implements AttributeConverter<TaskType, String> {

    @Override
    public TaskType convertToEntityAttribute(String code) {
        return Stream.of(TaskType.values())
                     .filter(taskType -> code.equals(taskType.getCode()))
                     .findFirst()
                     .orElseThrow(() -> new IllegalArgumentException(code + " is not a valid task type"));
    }

    @Override
    public String convertToDatabaseColumn(TaskType taskType) {
        return taskType.getCode();
    }
}
