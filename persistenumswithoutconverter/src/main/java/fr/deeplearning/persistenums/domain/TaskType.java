package fr.deeplearning.persistenums.domain;

public enum TaskType {

    FRONT,
    BACK,
    RUNDEV,
    OPS;
}
